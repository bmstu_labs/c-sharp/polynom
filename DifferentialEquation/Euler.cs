﻿using Points;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Differential;
public class Euler
{
    public static Point2D[] getPoints(Func<double, double, double> func, double step, Point2D StartPoint, double Max)
    {
        int n = (int)((Max - StartPoint.X) / step);

        Point2D[] res = new Point2D[n];

        res[0] = StartPoint;

        for (int i = 1; i < n; i++)
        {
            var x = res[i - 1].X + step;
            var y = res[i - 1].Y + step * func(res[i - 1].X, res[i - 1].Y);
            res[i] = new Point2D(x, y);
        }

        return res;
    }
}
