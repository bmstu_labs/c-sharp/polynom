﻿using Points;
using System.Collections;

namespace Differential;

public static class EnumerableExt
{
    public static IEnumerable<T> Odd<T>(this IEnumerable<T> e)
    {
        int i = 0;
        foreach (T item in e)
            if (++i %2 == 0)
                yield return item;
                //yield return e.Where;
    }
}


class Fib : IEnumerable<int>
{
    private int prev = 0;
    private int cur = 1;
    private int max = 1;

    public Fib(int Max) => max = Max;
    public IEnumerator<int> GetEnumerator()
    {
        while (cur < max)
        {
            (prev, cur) = (cur, prev + cur);
            yield return prev;
        } 
    }

    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
}



internal class Program
{
    public class pep
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
    }
    static void Main(string[] args)
    {    
        double analytic(double y) => 3 * Math.Exp(y) - y * y - 2 * y - 2;

        double pikar1(double x) =>
            1
            + Math.Pow(x, 3) / 3
            + x;
        
        double pikar2(double x) =>
            1
            + Math.Pow(x, 3) / 3
            + Math.Pow(x, 4) / 12
            + Math.Pow(x, 2) / 2
            + x;

        double pikar3(double x) =>
            1
            + Math.Pow(x, 3) / 3
            + Math.Pow(x, 4) / 12
            + Math.Pow(x, 5) / 60
            + Math.Pow(x, 3) / 6
            + Math.Pow(x, 2) / 2
            + x;

        double pikar4(double x) =>
            1
            + Math.Pow(x, 3) / 3
            + Math.Pow(x, 4) / 12
            + Math.Pow(x, 5) / 60
            + Math.Pow(x, 6) / 360
            + Math.Pow(x, 4) / 24            
            + Math.Pow(x, 3) / 6
            + Math.Pow(x, 2) / 2
            + x;

        double max = 2;

        var points1 = Euler.getPoints((x, y) => x * x + y, 0.10, new Point2D(0, 1), max);
        var points2 = Euler.getPoints((x, y) => x * x + y, 0.01, new Point2D(0, 1), max);

        var xx = Enumerable.Range(0, 10).Select(x => x * 0.1).ToArray();

        Console.WriteLine("|         x |  Analytic |  Euler0.1 | Euler0.01 |    Pikar1 |    Pikar2 |    Pikar3 |    Pikar4 |");
        Console.WriteLine("| --------- | --------- | --------- | --------- | --------- | --------- | --------- | --------- |");

        int i = 0;
        foreach (var point in points1)
            Console.WriteLine(
                $"| {point.X,9:#0.000000} " +
                $"| {analytic(point.X),9:#0.000000} " +
                $"| {point.Y,9:#0.000000} " +
                $"| {points2[i++ *10].Y,9:#0.000000} " +
                $"| {pikar1(point.X),9:#0.000000} " +
                $"| {pikar2(point.X),9:#0.000000} " +
                $"| {pikar3(point.X),9:#0.000000} " +
                $"| {pikar4(point.X),9:#0.000000} |");
    }
}
