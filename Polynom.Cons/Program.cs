﻿using Points;

namespace Polynom.Cons;
/*public class A : IDisposable
{
    public void Dispose() {; }

    public void f()
    {
        try
        {
            throw new Exception("asdf");
        }
        catch
        {
            ;
        }
    }
}*/



internal class Program
{
    static void Main(string[] args)
    {
        /*A a = new A();
        a.f();
        a.Dispose();*/


        var data = /*PointsArrayExtensionts.FromString(File.ReadAllText("data1.pts"));
        Console.WriteLine(data);*/
            new Point2D[] {
                new Point2D( 0.05, 6730 ),
                new Point2D(    1, 6790 ),
                new Point2D(    5, 7150 ),
                new Point2D(   10, 7270 ),
                new Point2D(   50, 8010 ),
                new Point2D(  200, 9185 ),
                new Point2D(  400, 10010 ),
                new Point2D(  800, 11140 ),
                new Point2D( 1200, 12010 )
            };

        var n = new Newton2D(data);

        var d = n.Interpolate( 1000 , 3 );
        
        /*
        Console.WriteLine("Выберите задание:");
        Console.WriteLine("  1 - интерполяция");
        Console.WriteLine("  2 - поиск корня");
        Console.WriteLine("  3 - решение системы");
        var k = int.Parse(Console.ReadKey().KeyChar.ToString());
        switch (k)
        {
            case 1:
                {

                    break;
                }
        }*/
    }
}
