﻿namespace Polynom;

public static class PointsArrayExtensionts
{
    public static string AsString(this double[][] array) =>
        string.Join((char)13, array.Select(p => string.Join(' ', p)));

    public static string AsString(this double[] array) =>
       string.Join(' ', array);

    public static string AsString(this int[] array) =>
       string.Join(' ', array);

    public static double[][] FromString(string? data)
    {
        if (string.IsNullOrEmpty(data))
            return null;

        List<double[]> result = new();

        var d1 = data.Split((char)13);
        foreach (var d2 in d1)
            result.Add(d2.Split(' ').Select(x => double.Parse(x.Replace('.', ',')/*, CultureInfo.InvariantCulture*/)).ToArray());

        return result.ToArray();
    }
}
