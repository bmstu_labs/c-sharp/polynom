using Microsoft.AspNetCore.Mvc;

namespace Polynom.API.Controllers;
[ApiController]
[Route("[controller]")]
public class PolynomController : ControllerBase
{
    private readonly ILogger<PolynomController> _logger;

    public PolynomController(ILogger<PolynomController> logger) =>
        _logger = logger;

    [HttpPost("interpolate")]
    public double Get([FromBody] InterpolationData data) =>
        new NewtonBase(data.Points).Interpolate(data.InterpolationPoint, data.Degree);
/*
    [HttpPost("interpolate")]
    public double Get() =>
        5.5;*/
}