﻿namespace Polynom.API;

public class InterpolationData
{
    public double[][] Points { get; set; }
    public double[] InterpolationPoint { get; set; }
    public int[] Degree { get; set; }
}
