﻿using Points;
using System.Runtime.InteropServices;

namespace Polynom.Tests;

public class NewtonTests
{
    private readonly Newton2D n = new(new Point2D[]{
        new Point2D(-0.50, 0.707 ),
        new Point2D(-0.25, 0.924 ),
        new Point2D( 0.00, 1.000 ),
        new Point2D( 0.25, 0.924 ),
        new Point2D( 0.50, 0.707 ),
        new Point2D( 0.75, 0.383 ),
        new Point2D( 1.00, 0.000 )
    });

    [Fact]
    public void TestFirstDegreeIsAvg()
    {
        for (int i = 0; i < n.Points.Length - 2; i++)
        {
            Assert.Equal((n.Points[i].Y + n.Points[i + 1].Y) / 2.0,
                n.Interpolate((n.Points[i].X + n.Points[i + 1].X) / 2.0, 1));
        }
    }
    
    [Fact]
    public void TestX06()
    {
        double[] Expected = new double[] {
            0.5774,
            0.59024,
            0.587552,
            0.5878656,
            0.587768832,
            0.587707904
        };

        for (int i = 1; i < 7; i++)
            Assert.Equal(Expected[i - 1], n.Interpolate(0.6, i), 0.0000000001);

        for (int i = 1; i < 7; i++)
            Assert.Equal(0.588, n.Interpolate(0.6, 0.001), 0.001);

        // с экстраполяцией
        //Assert.Equal(-0.805, NewtonPolynom.Interpolate(points, 1.5, new double[] { 6 }, true), 0.0001);
    }
    
    [Fact]
    public void TestX05() =>
        Assert.Equal(0.707, n.Interpolate(0.5, 4), 0.0000000001);
    
    [Fact]
    public void TestFindX()
    {   
        var points = Enumerable.Range(0, 50).Select(i => new Point2D(i/10D, Math.Pow(i/10D, 2))).ToArray();
        var n = new Newton2D(points);
        Assert.Equal(6.25, n.Interpolate(2.5, 0.001), 0.001);
    }

    [Fact]
    public void TestFindY()
    {
        var points = Enumerable.Range(0, 50).Select(i => new Point2D(i / 10D, Math.Pow(i / 10D, 2))).ToArray();
        var new_points = points.Select(p => new Point2D(p.Y, p.X)).ToArray();
        var n = new Newton2D(new_points);
        Assert.Equal(2.5, n.Interpolate(6.25, 0.001), 0.001);
    }

    [Fact]
    public void Test3D()
    {
        Newton3D n = new(new Point3D[] {
            new Point3D( 1, 1.2, 0.606 ),
            new Point3D( 1, 1.6, 0.449 ),
            new Point3D( 1.4, 1.2, 0.682 ),
            new Point3D( 1.4, 1.6, 0.532 ),
        });        
        Assert.Equal(0.53903, n.Interpolate(new Point2D(1.25, 1.5), new int[] { 1, 1 }), 0.00001);
    }

    [Fact]
    public void Test_3D_3ikskvadrat_plus_2igrek_plus5()
    {
        double z(double x, double y) { return 3*x*x + 2*y + 5; }
        
        List<Point3D> points = new();
        for (var x = -5D; x <= 5; x += 0.1)
            for (var y = -2D; y <= 2; y += 0.1)
                points.Add(new(x, y, z(x,y)));
        
        Newton3D n = new(points.ToArray());

        var x0 = 0.5;
        var y0 = 0;
        var z0 = z(x0, y0);
        var iPoint = new Point2D(x0, y0);
        Assert.Equal(z0, n.Interpolate(iPoint, 0.00001), 0.00001);
    }

    /*

    [Fact]
    public void TestThrows()
    {
        Assert.Throws<ArgumentOutOfRangeException>(() => n.GetY(0.6, 0));
        Assert.Throws<ArgumentOutOfRangeException>(() => n.GetY(0.6, 7));
        Assert.Throws<ArgumentException>(() => n.GetY(1.5, 6));
        Assert.Throws<ArgumentException>(() => n.GetY(-0.6, 6));
        Assert.Throws<ArgumentException>(() => new Newton(new Point2D[0]));
    }*/
}