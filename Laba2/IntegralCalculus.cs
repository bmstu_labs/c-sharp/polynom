﻿using System;

namespace Integrals;

/// <summary>
/// Метод (приблизительного) вычисления значения определенного интеграла
/// </summary>
public enum diCalcMethod
{
    /// <summary>
    /// Кусочно-постоянная аппроксимация (метод прямоугольников со средней точкой)
    /// </summary>
    /// <remarks></remarks>
    /// <see href="https://ru.wikipedia.org/wiki/Численное_интегрирование#Метод_прямоугольников"/>
    cmRect,

    /// <summary>
    /// Кусочно-линейная аппроксимация (метод трапеций)
    /// </summary>
    /// <see href="https://ru.wikipedia.org/wiki/Численное_интегрирование#Метод_трапеций"/>    
    cmTrap,

    /// <summary>
    /// Кусочно-параболическая аппроксимация (метод парабол)
    /// </summary>
    /// <see href="https://ru.wikipedia.org/wiki/Численное_интегрирование#Метод_парабол_(метод_Симпсона)"/>
    cmParab
};

public class IntegralCalculus
{
    /// <summary>
    /// Вычисляет определенный интеграл разбиение площади под кривой на n фигур
    /// </summary>
    /// <remarks>
    /// Вычисляет аппроксимированное значение определенного интеграла
    /// на интервале (<paramref name="a"/>,<paramref name="b"/>) значений абсциссы, 
    /// как сумму "площадей" n фигур под кривой, где <paramref name="n"/> -
    /// количество разбиений исходной площади.
    /// </remarks>
    /// <param name="func">Функция, задающая интегральную кривую</param>
    /// <param name="a">Начало интервала</param>
    /// <param name="b">Конец интервала</param>
    /// <param name="n">Количество разбиений</param>
    /// <param name="method">Метод вычисления значения интеграла <see cref="diCalcMethod"/></param>
    /// <returns>Приблизительное значение интеграла</returns>
    private static double Calculate(Func<double, double> func, double a, double b, int n, diCalcMethod method = diCalcMethod.cmTrap)
    {
        static double GetVal(Func<double, double> func, double a, double b, diCalcMethod method)
        {
            return method switch
            {
                diCalcMethod.cmRect => func((b + a) / 2) * (b - a),
                diCalcMethod.cmTrap => ((func(a) + func(b)) * (b - a)) / 2,
                _ => throw new NotImplementedException("Указанный метод неизвестен")
            };
        }

        double x = a;
        double dx = (b - a) / n;

        double value = 0;

        while (x + dx < b)
            value += GetVal(func, x, x += dx, method);

        value += GetVal(func, x, b, method);

        return value;
    }

    /// <summary>
    /// Вычисляет значение определенного интеграла с заданной точностью
    /// </summary>
    /// <remarks>
    /// Вычисляет аппроксимированное значение определенного интеграла
    /// на интервале (<paramref name="a"/>,<paramref name="b"/>) значений абсциссы, 
    /// с заданной точностью <paramref name="precision"/>
    /// </remarks>
    /// <param name="func">Функция, задающая интегральную кривую</param>
    /// <param name="a">Начало интервала</param>
    /// <param name="b">Конец интервала</param>
    /// <param name="precision">Требуемая точность</param>
    /// <param name="method">Метод вычисления значения интеграла <see cref="diCalcMethod"/></param>
    /// <returns>Приблизительное значение интеграла</returns>
    public static double Calculate(Func<double, double> func, double a, double b, double precision, diCalcMethod method = diCalcMethod.cmRect)
    {
        int split = 1;

        double square = Calculate(func, a, b, split, method);

        while (Math.Abs(square - (square = Calculate(func, a, b, split *= 2, method))) > precision)
            ;

        return square;
    }
}
