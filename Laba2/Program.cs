﻿using Points;
using Polynom;
using Integrals;
using System.Security.Cryptography;
using System;

namespace Laba2;

internal class Program
{
    public delegate double XYFunc(double X, double Y);

    static void Main(string[] args)
    {
        var Lk = 187 * 1e-6;
        var Ck = 268 * 1e-6;
        var Rk = 0.25;
        var Tw = 2000;
        var le = 12;

        var T0 = 0D;

        double sigma(double z, double I, double Tw)
        {
            var tab1 = new double[][]
            {
                  new double[]{ 0.5, 6730, 0.50 },
                  new double[]{ 1, 6790, 0.55 },
                  new double[]{ 5, 7150, 1.7 },
                  new double[]{ 10, 7270, 3 },
                  new double[]{ 50, 8010, 11 },
                  new double[]{ 200, 9185, 32 },
                  new double[]{ 400, 10010, 40 },
                  new double[]{ 800, 11140, 41 },
                  new double[]{ 1200, 12010, 39 },
            };

            var tab2 = new double[][]
            {
              new double[] { 0D, 0 },
              new double[] { 1000D, 0.001 },
              new double[] { 2000D, 0.011 },
              new double[] { 3000D, 0.021 },
              new double[] { 4000D, 0.031 },
              new double[] { 5000D, 0.27 },
              new double[] { 6000D, 2.05 },
              new double[] { 7000D, 6.06 },
              new double[] { 8000D, 12.0 },
              new double[] { 9000D, 19.9 },
              new double[] { 10000D, 29.6 },
              new double[] { 11000D, 41.1 },
              new double[] { 12000D, 54.1 },
              new double[] { 13000D, 67.7 },
              new double[] { 14000D, 81.5 },
            };

            var t1 = tab1.Select((item) => new double[2] { item[0], item[1] }).ToArray();
            var t2 = tab1.Select((item) => new double[2] { item[0], item[2] }).ToArray();
            var t3 = tab2.Select((item) => new double[2] { item[0], item[1] }).ToArray();

            var t0 = new NewtonBase(t1).Interpolate(new double[] { I }, new int[] { 4 } );
            T0 = t0;
            var m = new NewtonBase(t2).Interpolate(new double[] { I }, new int[] { 4 } );
            var T = T0 + (Tw - T0) * Math.Pow(z, m);
            var n = new NewtonBase(t3);
            var sigma = n.Interpolate(new double[] { T }, new int[] { 4 } );
            return sigma;
        }

        double Rp(double I, double T)
        {
            (int a, int b) = (0, 1);
            var n = 100;
            var dz = (b - a) / n;
            var intgr = 0D;
            var z = 0;
            var R = 0.35;

            intgr = IntegralCalculus.Calculate((z) => sigma(z, I, Tw)*z, 0, 1, 0.001, diCalcMethod.cmTrap);

            return le / (2 * Math.PI * R * R * intgr);
        }

        Func<double, double> dUdt = (I) => -I / Ck;

        Func<double, double, double> dIdt = (I, U) =>
            (U - (Rk + Rp(I, Tw)) * I) / Lk;

        Point2D IU(double I, double U, double h)
        {
            var k1 = h * dIdt(I, U);
            var p1 = h * dUdt(I);

            var k2 = h * dIdt(I + k1 / 2, U + p1 / 2);
            var p2 = h * dUdt(I + k1 / 2);

            var k3 = h * dIdt(I + k2 / 2, U + p2 / 2);
            var p3 = h * dUdt(I + k2 / 2);

            var k4 = h * dIdt(I + k3, U + p3);
            var p4 = h * dUdt(I + k3);

            var point = new Point2D(I + (k1 + 2 * k2 + 2 * k3 + k4) / 6 ,
                                    U + (p1 + 2 * p2 + 2 * p3 + p4) / 6 );
            return point;
        }

        double RungeKutta4(XYFunc Xfunc, XYFunc Yfunc, double x0, double y0, double h)
        {
            var ky1 = h * Yfunc(x0, y0); // 5.1151474720690713
            var kx1 = h * Xfunc(x0, y0); // -0.0018656716417910447

            var ky2 = h * Yfunc(x0 + (h/2) * Xfunc(x0, y0), y0 + (h/2) * ky1); // 5.6822845621242708
            var kx2 = h * Xfunc(x0 + ky1/2, y0 + kx1/2); // -0.011408857224009462

            var ky3 = h * Yfunc(x0 + (h/2) * Xfunc(x0, y0), y0 + h * ky2); // 5.8979738491447193
            var kx3 = h * Xfunc(x0 + ky2 / 2, y0 + kx2 / 2); // -0.012466948809933341

            var ky4 = h * Yfunc(x0 + h * Xfunc(x0, y0), y0 + h * ky3); // 6.7213139022504231
            var kx4 = h * Xfunc(x0 + ky3, y0 + kx3); // -0.023873036750539995

            return y0 + (ky1 + 2.0 * ky2 + 2.0 * ky3 + ky4) / 6.0;
        }

        var Uc = 1400D;
        var I = 0.5;

        var h = 1e-6;
        var t = 0D;
        var tmax = 1e-5;

        /*Console.WriteLine($"{0.5,9:#0.0000} {Rp(0.5, Tw),9:#0.0000}");
        
        foreach (var ddd in new double[] { 0.5, 5.412, 12.003, 18.714, 25.398, 32.032, 38.611, 45.136, 51.608, 58.03 })
            Console.WriteLine($"{ddd,9:#0.0000} {Rp(ddd, Tw),9:#0.0000} {T0,9:#0.000}");
        return;*/

        var II = I;
        var UU = Uc;
        while (t < tmax)
        {
            XYFunc dU = (I, U) => -I / Ck;

            XYFunc dI = (U, I) => (U - (Rk + Rp(I, Tw)) * I) / Lk;

            var p = IU(I, Uc, h);

            var UUU = calculatedU(I, Uc, h);
            var III = calculatedI(Uc, I, h);

            UU = RungeKutta4(dU, dI, I, Uc, h);

            II = RungeKutta4(dI, dU, Uc, I, h);

            I = p.X;
            Uc = p.Y;
            Console.WriteLine($"{t,9:#0.00000000} {p.X,9:#0.000} {p.Y,9:#0.000}");
            Console.WriteLine($"{t,9:#0.00000000} {II,9:#0.000} {UU,9:#0.000}");

            t += h;            
        }

        double Ufunc(double I0, double U0) => -I0 / Ck;

        double Ifunc(double U0, double I0) =>
            (U0 - (Rk + Rp(I0, Tw)) * I0) / Lk;

        double calculatedU(double Ii, double Ui, double h)
        {
            double k0 = Ufunc(Ii, Ui);
            double k1 = Ufunc(Ii + (h / 2) * Ifunc(Ui, Ii), Ui	+	(h/2)	*	k0); // -11408.857224009462
            double k2 = Ufunc(Ii + (h / 2) * Ifunc(Ui, Ii), Ui	+	(h/2)	*	k1); // -11408.857224009462
            double k3 = Ufunc(Ii + h * Ifunc(Ui, Ii), Ui + h * k2);
            return Ui + (h / 6) * (k0 + 2 * k1 + 2 * k2 + k3);
        }

        double calculatedU1(double x0, double y0, double h)
        {
            double k0 = Ufunc(x0, y0);
            double k1 = Ufunc(x0 + (h / 2) * Ifunc(y0, x0), y0 + (h / 2) * k0); // -11408.857224009462
            double k2 = Ufunc(x0 + (h / 2) * Ifunc(y0, x0), y0 + (h / 2) * k1); // -11408.857224009462
            double k3 = Ufunc(x0 + h * Ifunc(y0, x0), y0 + h * k2);
            return y0 + (h / 6) * (k0 + 2 * k1 + 2 * k2 + k3);
        }

        double calculatedI(double Ui, double Ii, double h)
        {
            double k0 = Ifunc(Ui, Ii);                                                 // 5115147.4720690716
                                                                                       // 5115147.4720690713 
            double kl = Ifunc(Ui + (h / 2)	* Ufunc(Ii, Ui),	Ii	+	(h/2)	* k0); // 5682284.5621242709
                                                                                       // 5682284.5621242708 
            double k2 = Ifunc(Ui + (h / 2)	* Ufunc(Ii, Ui),	Ii	+	(h/2)	* kl); // 5897999.3656837093
                                                                                       // 5897973.8491447193 
            double k3 = Ifunc(Ui + h * Ufunc(Ii, Ui), Ii + h* k2);                     // 6721372.3409910994
                                                                                       // 6721313.9022504231
            return Ii + (h / 6) * (k0 +2* kl+2* k2+ k3);
        }

        // 5.1151474720690713
        // -0.0018656716417910447

        // 5.6822845621242708
        // -0.011408857224009462

        // 5.8979738491447193
        // -0.012466948809933341

        // 6.7213139022504231
        // -0.023873036750539995

        /*
                res_I.append(I)
                res_U.append(Uc)
                res_t.append(t)
                res_T0.append(T0)
                res_Rp.append(Rp)

            for i in range(len(res_Rp)):
                res_IRp.append(res_I[i] * res_Rp[i])

            show_graph()

            #print(Rk, find_length_p())*/

        /*$([...Array(5).keys()]).each(
            (i) => alert(i));*/

        /*let t = runge_kutta_4(dUdt, 1, 2, undefined);
        alert(t);*/
    }
}
