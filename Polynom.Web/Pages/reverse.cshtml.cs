﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace Polynom.Web.Pages;

[BindProperties]
public class ReverseModel : PageModel
{
    private readonly ILogger<ReverseModel> _logger;

    public ReverseModel(ILogger<ReverseModel> logger) => _logger = logger;

    private NewtonBase? newton { get; set; } = null!;

    [Display(Name ="Корень функции")]
    public decimal? InterpolationResult { get; private set; }

    [Display(Name = "Массив известных точек")]
    [Required(ErrorMessage = "Укажите массив известных точек")]
    public string Points
    {
        set => newton = new(
            PointsArrayExtensionts.FromString(value)
            .Select(p => new double[2] { p[1], p[0] })
            .OrderBy(p => p[0])
            .ToArray()
        );
        get => newton == null ? "" : newton.Points.AsString();
    }

    [Display(Name = "Значение функции")]
    [Required(ErrorMessage= "Укажите значение функции")]
    public double[] IPoint { get; set; } = null!;

    [Display(Name = "Степень(и) полинома")]
    [Required(ErrorMessage = "Укажите степень(и) полинома", AllowEmptyStrings=true)]
    public int[] Degree { get; set; } = null!;

    //string? points, string? ipoint, string? degree
    public void OnPost()
    {
        var result = newton?.Interpolate(IPoint, Degree);
        if (result != null)
            InterpolationResult = Math.Round((decimal)result, 6);
        else
            InterpolationResult = null;
    }
}