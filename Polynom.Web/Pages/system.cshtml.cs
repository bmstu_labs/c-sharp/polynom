﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace Polynom.Web.Pages;

[BindProperties]
public class SystemModel : PageModel
{
    private readonly ILogger<SystemModel> _logger;

    public bool Extrapolation => newton?.Extrapolation ?? false;

    public SystemModel(ILogger<SystemModel> logger) => _logger = logger;

    private NewtonBase? newton { get; set; } = null!;

    [Display(Name = "Результат интерполяции")]
    public decimal? InterpolationResult { get; private set; }

    [Display(Name = "Массив известных точек")]
    [Required(ErrorMessage = "Укажите массив известных точек")]
    public string Points
    {
        set => newton = new(PointsArrayExtensionts.FromString(value));
        get => newton == null ? "" : newton.Points.AsString();
    }

    [Display(Name = "Второй массив известных точек")]
    [Required(ErrorMessage = "Укажите второй массив известных точек")]
    public string Points2 { get; set; }

    [Display(Name = "Точка интерполяции")]
    [Required(ErrorMessage="Укажите точку интерполяции")]
    public double[] IPoint { get; set; } = null!;

    [Display(Name = "Степень(и) полинома")]
    [Required(ErrorMessage = "Укажите степень(и) полинома", AllowEmptyStrings=true)]
    public int[] Degree { get; set; } = null!;

    //string? points, string? ipoint, string? degree
    public void OnPost()
    {
        var result = newton?.Interpolate(IPoint, Degree);
        if (result != null)
            InterpolationResult = Math.Round((decimal)result, 6);
        else
            InterpolationResult = null;
    }
}