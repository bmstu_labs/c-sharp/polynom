﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace Polynom.Web.Pages;

[BindProperties]
public class PreciseModel : PageModel
{
    private readonly ILogger<PreciseModel> _logger;

    public bool Extrapolation => newton?.Extrapolation ?? false;

    public PreciseModel(ILogger<PreciseModel> logger) => _logger = logger;

    private NewtonBase? newton { get; set; } = null!;

    [Display(Name = "Результат интерполяции")]
    public decimal? InterpolationResult { get; private set; }

    [Display(Name = "Массив известных точек")]
    [Required(ErrorMessage = "Укажите массив известных точек")]
    public string Points
    {
        set => newton = new(PointsArrayExtensionts.FromString(value.Replace('.', ',')));
        get => newton == null ? "" : newton.Points.AsString();
    }

    [Display(Name = "Точка интерполяции")]
    [Required(ErrorMessage="Укажите точку интерполяции")]
    public double[] IPoint { get; set; } = null!;

    [Display(Name = "Требуемая точность")]
    [Required(ErrorMessage = "Укажите требуемую точность", AllowEmptyStrings=true)]
    public string Accuracy { get; set; }

    //string? points, string? ipoint, string? degree
    public void OnPost()
    {
        var result = newton?.Interpolate(IPoint, double.Parse(Accuracy.Replace('.', ',')));
        if (result != null)
            InterpolationResult = Math.Round((decimal)result, 6);
        else
            InterpolationResult = null;
    }
}