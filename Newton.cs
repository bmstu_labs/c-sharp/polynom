﻿using Points;

namespace Polynom;

public class NewtonBase
{
    public double[][] Points { get; private set; }

    public bool Extrapolation { get; set; }

    public NewtonBase(double[][] points) => Points = points;

    private double Interpolate(double[][] pts, double[] iPoint, int[] Degree)
    {
        checked
        { 
            if (pts.Length == 0)
                throw new ArgumentException("Исходные данные не заданы");

            if (pts[0].Length > 2)
            {
                var tmp = pts
                    .GroupBy(p => p[0])
                    .Select(g => new { X = g.Key, Points = g.Select(a => a[1..]).ToArray() })
                    .Select(g => new double[2] { g.X, Interpolate(g.Points, iPoint[1..], Degree[1..]) })
                    .ToArray();

                return Interpolate(tmp, iPoint[0..1], Degree[0..1]);
            }

            var max = pts.Max(p => p[0]);
            var min = pts.Min(p => p[0]);
            Extrapolation = iPoint[0] > max || iPoint[0] < min;

            double[][] GetFrame(double[][] Points, double Center, int Qnt)
            {
                if (Qnt < 1 || (Points.Length < Qnt + 1))
                    throw new ArgumentOutOfRangeException(
                        $"Polynom degree should be in the range from 1 to {Points.Length - 1}");

                int idx = 0;
                for (int i = 0; i < Points.Length; i++)
                {
                    if (Points[i][0] >= Center)
                    {
                        idx = i;
                        break;
                    }
                    idx = i;
                }
                if (idx != 0 && Math.Abs(Points[idx - 1][0] - Center) <= Math.Abs(Points[idx][0] - Center))
                    idx--;
                /*
                (int l, int h) = (idx, idx);

                for (int i = 0; i < Qnt; i++)
                {
                    if (i % 2 == 0)
                    {
                        if (l > 0)
                        {
                            l--;
                            continue;
                        }
                    }
                    else
                    {
                        if (h < Points.Length - 1)
                        {
                            h++;
                            continue;
                        }
                    }
                    if (l > 0)
                        l--;
                    if (h < Points.Length - 1)
                        h++;
                }

                var frame = Points[l..(h+1)];

                var frame1 = Points
                   .OrderBy(p => Math.Abs(p[0] - Center))
                   .Take(Qnt + 1)
                   .OrderBy(p => p[0])
                   .ToArray(); // плохо, т.к. могут взяться с однгой стороны, если неравномерно 
                

                
                int closest_idx = Points
                   .Select((p, i) => new { point = p, idx = i })
                   .OrderBy(p => Math.Abs(p.point[0] - Center))
                   .Take(1)
                   .FirstOrDefault()!.idx;

                if (idx != closest_idx)
                    throw new Exception("asdf");*/

                var down = (int)(Points[idx][0] < Center ? Math.Floor(Qnt / 2.0) : Math.Ceiling(Qnt / 2.0));
                var up = (int)(Points[idx][0] < Center ? Math.Ceiling(Qnt / 2.0) : Math.Floor(Qnt / 2.0));
                if (0 > (int)(idx - down))
                {
                    up = up + (int)(down - idx);
                    down = idx;
                }
                else
                //var si = Math.Max(0, (int)(idx - down));

                if (Points.Length - 1 < (int)(idx + up))
                {
                    down = down + ((int)(idx + up) - (Points.Length - 1));
                    up = up - ((int)(idx + up) - (Points.Length - 1));
                }

                /*var ei = Math.Min(Points.Length - 1, (int)(idx + up));
                if (0 > (int)(idx - down))
                    ei = ei + (int)(down - idx);
                if (Points.Length - 1 < (int)(idx + up))
                    si = si - (int)(idx + up - (Points.Length - 1));*/

                return Points[(idx - down)..(idx + up + 1)];
            }

            pts = GetFrame(pts, iPoint[0], Degree[0]);

            double[,] Y = new double[pts.Length, pts.Length];

            for (int i = 0; i < pts.Length; i++)
                Y[0, i] = pts[i][1];

            for (int i = 0; i < pts.Length - 1; i++)
                for (int j = 0; j < pts.Length - i - 1; j++)
                    Y[i + 1, j] = (Y[i, j] - Y[i, j + 1]) / (pts[j][0] - pts[j + 1 + i][0]);

            var p = 0D;

            for (int i = 0; i < pts.Length; i++)
            {
                var pr = 1D;

                for (int j = 0; j < i; j++)
                    pr *= iPoint[0] - pts[j][0];

                p += pr * Y[i, 0];
            }

            return p;
        }
    }

    public virtual double Interpolate(double[] iPoint, int[] Degree) =>
        Interpolate(Points, iPoint, Degree);

    public double Interpolate(double[] iPoint, double accuracy)
    {
        var a = new int[iPoint.Length];
        Array.Fill(a, 1);
        double y = Interpolate(iPoint, a);
        
        double tmp;
        int i = 1;

        while (++i < Points.GroupBy(p => p[0]).Count() - 1)
        {
            a = new int[iPoint.Length];
            Array.Fill(a, i);
            tmp = Interpolate(iPoint, a);
            if (Math.Abs(y - tmp) <= accuracy)
                return y;

            y = tmp;
        }

        throw new ApplicationException("Polynom can not be calculated with specified accuracy");
    }
}

public class Newton2D : NewtonBase
{
    public Newton2D(Point2D[] points) : base(points.Select(p => new double[] { p.X, p.Y }).ToArray()) {; }

    public double Interpolate(double X, int Degree) =>
        base.Interpolate(new double[] { X }, new int[] { Degree });

    public double Interpolate(double iPoint, double accuracy) =>
        base.Interpolate(new double[] { iPoint }, accuracy);

    public new Point2D[] Points =>
        base.Points.Select(p => new Point2D(p[0], p[1])).ToArray();
}

public class Newton3D : NewtonBase
{
    public Newton3D(Point3D[] points) : base(points.Select(p => new double[] { p.X, p.Y, p.Z }).ToArray()) {; }

    public double Interpolate(Point2D iPoint, int DegreeX, int DegreeY) =>
        base.Interpolate(new double[] { iPoint.X, iPoint.Y }, new int[] { DegreeX, DegreeY});

    public double Interpolate(Point2D iPoint, double accuracy) =>
        base.Interpolate(new double[] { iPoint.X, iPoint.Y }, accuracy);

    public new Point3D[] Points =>
        base.Points.Select(p => new Point3D(p[0], p[1], p[2])).ToArray();
}