﻿using System.Drawing;

namespace Points;
public struct Point2D
{
    public double X;
    public double Y;
    public Point2D(double x, double y) => (X,Y) = (x,y);
    

    public Point ToPoint() => 
        new Point((int)X, (int)Y);

    public override bool Equals(object? obj) =>
        obj is Point2D && this == (Point2D)obj;

    public override int GetHashCode() =>
        X.GetHashCode() ^ Y.GetHashCode();

    public static bool operator ==(Point2D a, Point2D b) =>
        a.X == b.X && a.Y == b.Y;

    public static bool operator !=(Point2D a, Point2D b) =>
        !(a == b);

    public static implicit operator double[](Point2D point)
        => new double[] { point.X, point.Y };
}
