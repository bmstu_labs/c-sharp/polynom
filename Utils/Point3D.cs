﻿namespace Points;
public struct Point3D
{
    public double X;
    public double Y;
    public double Z;
    public Point3D(double x, double y, double z)
    {
        X = x;
        Y = y;
        Z = z;
    }

    public override bool Equals(object? obj)
    {
        return obj is Point3D && this == (Point3D)obj;
    }
    public override int GetHashCode()
    {
        return X.GetHashCode() ^ Y.GetHashCode();
    }
    public static bool operator ==(Point3D a, Point3D b)
    {
        return a.X == b.X && a.Y == b.Y && a.Z == b.Z;
    }
    public static bool operator !=(Point3D a, Point3D b)
    {
        return !(a == b);
    }
}
